SHELL=bash

app-build:
	docker build application/ -t sre-task-kkovalev:dev

app-test: app-build
	docker run \
		--rm -ti \
		--network host \
		-e HELLO_TEST=1 \
		sre-task-kkovalev:dev \
		nosetests -v --with-doctest ./ ./hello

app-release: app-test
	docker tag sre-task-kkovalev:dev agrrh/hello-kkovalev:stable
	docker push agrrh/hello-kkovalev:stable

docs-build:
	docker build docs/ -t sre-task-kkovalev:dev-docs

docs-release: docs-build
	docker tag sre-task-kkovalev:dev-docs agrrh/hello-kkovalev:stable-docs
	docker push agrrh/hello-kkovalev:stable-docs

docs-run: docs-build
	docker run --rm -ti --name sre-task-kkovalev-dev-docs \
		--publish 5001:3000 sre-task-kkovalev:dev-docs

warning-print:
	@echo "! Not executing directly to evade possible damage of your environment"
	@echo ""
	@echo "Also please don't forget to fix database address:"
	@echo "  sed -i 's/value: 127.0.0.1/value: <proper-database-ip>/g' application/hello-kkovalev-k8s.yml"
	@echo ""

app-deploy-local: warning-print
	@echo "Proposed way to deploy application locally:"
	@echo "$$ kubectl config use-context minikube"
	@echo "$$ kubectl apply -f application/redis.k8s.yml"
	@echo "$$ kubectl apply -f application/hello-kkovalev.k8s.yml"
	@echo "$$ minikube service hello-kkovalev"
	@echo ""
	@echo "Alternative method:"
	@echo "$$ docker-compose up"
	@echo "$$ curl 0.0.0.0:5000/health"

app-deploy-cloud: warning-print
	@echo "Proposed way to deploy application locally:"
	@echo "$$ gcloud container clusters get-credentials main-gke-cluster \
  --zone europe-west2-a \
  --project \$${TF_VAR_project}"
	@echo "$$ kubectl apply -f application/hello-kkovalev.k8s.yml"
	@echo "$$ kubectl rollout status deployment/hello-kkovalev"
	@echo "$$ kubectl get service hello-kkovalev"

app-update: warning-print
	@echo "$$ kubectl set image deployment/hello-kkovalev api=agrrh/hello-kkovalev:stable --record"
	@echo "$$ kubectl rollout status deployment/hello-kkovalev"
