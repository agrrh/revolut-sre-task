variable "org_id" {
  description = "Organization ID, see `gcloud organizations list`"
  type        = string
}

variable "billing_account" {
  description = "Billing account ID, see https://console.cloud.google.com/billing/projects or `gcloud alpha billing accounts list`"
  type        = string
}
