resource "null_resource" "redis_enable_delay" {
  // Error: googleapi: Error 403: Kubernetes Engine API has not been used in project XXXXXXXXXXXX before or it is disabled. Enable it by visiting https://console.developers.google.com/apis/api/container.googleapis.com/overview?project=XXXXXXXXXXXX then retry. If you enabled this API recently, wait a few minutes for the action to propagate to our systems and retry., accessNotConfigured

  provisioner "local-exec" {
    command = "test -f /tmp/.redis_enable_delay.done || sleep 60; touch /tmp/.redis_enable_delay.done"
  }

  depends_on = [
    google_project_service.service
  ]
}

resource "google_redis_instance" "database" {
  project = google_project.project.project_id

  name           = "database"
  memory_size_gb = 1

  depends_on = [
    null_resource.redis_enable_delay
  ]
}
output "database_host" {
  value = google_redis_instance.database.host
}

output "database_port" {
  value = google_redis_instance.database.port
}
