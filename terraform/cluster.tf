resource "null_resource" "gke_enable_delay" {
  // Error: googleapi: Error 403: Kubernetes Engine API has not been used in project XXXXXXXXXXXX before or it is disabled. Enable it by visiting https://console.developers.google.com/apis/api/container.googleapis.com/overview?project=XXXXXXXXXXXX then retry. If you enabled this API recently, wait a few minutes for the action to propagate to our systems and retry., accessNotConfigured

  provisioner "local-exec" {
    command = "test -f /tmp/.gke_enable_delay.done || sleep 60; touch /tmp/.gke_enable_delay.done"
  }

  depends_on = [
    google_project_service.service
  ]
}

resource "google_container_cluster" "primary" {
  project = google_project.project.project_id

  name = "main-gke-cluster"

  initial_node_count = 3

  ip_allocation_policy {}

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  depends_on = [
    null_resource.gke_enable_delay
  ]
}

output "kubernetes_endpoint" {
  value = google_container_cluster.primary.endpoint
}
