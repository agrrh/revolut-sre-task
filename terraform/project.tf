resource "random_id" "id" {
  byte_length = 4
  prefix      = "kkovalev-project-"
}

resource "google_project" "project" {
  name            = random_id.id.hex
  project_id      = random_id.id.hex
  org_id          = var.org_id
  billing_account = var.billing_account
}

resource "google_project_service" "service" {
  project = google_project.project.project_id

  for_each = toset([
    "compute.googleapis.com",
    "container.googleapis.com",
    "cloudapis.googleapis.com",
  ])

  service = each.key
}

output "project_id" {
  value = google_project.project.project_id
}
