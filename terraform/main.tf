provider "google" {
  # NOTE Use GOOGLE_APPLICATION_CREDENTIALS env variable to point to credentials file

  region = "europe-west2"
  zone   = "europe-west2-a"
}
