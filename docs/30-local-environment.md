# Local environment

It's available in two variants, check script output:

```bash
$ make app-deploy-local
```

## Disclaimer

Since, I've failed to fit in time, to properly deploy, one need to manually change `HELLO_STORAGE_HOST` variable in manifest file before running `kubectl apply`.

My generic approach with Nomad is to use one of:

- [Levant](https://github.com/jrasell/levant) templating features
- [consul-template](https://github.com/hashicorp/consul-template) run before applying manifests
- Sometimes even good old `sed`

I got no time to solve this templating problem here, sorry for that. I guess correct way in k8s world is [ConfigMap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/).

## 1. minikube

This is preferred method to bring things up since same manifest could be used for local and cloud (stage, uat, prod) environments.

Assuming one have `minikube` installed.

Running environment:

```bash
$ kubectl config use-context minikube
...

$ kubectl apply -f application/redis.k8s.yml

$ minikube service list

# Here it is, shame on me
$ sed -i 's/value: 127.0.0.1/value: <proper-database-ip>/g' application/hello-kkovalev-k8s.yml

$ kubectl apply -f application/hello-kkovalev.k8s.yml
...

$ minikube service list
|-----------|----------------|-------------|-----------------------------|
| NAMESPACE |      NAME      | TARGET PORT |             URL             |
|-----------|----------------|-------------|-----------------------------|
| default   | hello-kkovalev | http        | http://192.168.99.???:????? |
|-----------|----------------|-------------|-----------------------------|
```

## 2. docker-compose

Fallback method for those who don't have `minikube` installed, but have `docker-compose` instead.

Running environment:

```bash
$ docker-compose up
...
database_1  | 1:M 17 Jan 2020 06:59:50.643 * Ready to accept connections
app_1       | [2020-01-17 06:59:52 +0000] [1] [INFO] Listening at: http://0.0.0.0:5000 (1)
app_1       | [2020-01-17 06:59:52 +0000] [1] [INFO] Using worker: sync
app_1       | [2020-01-17 06:59:52 +0000] [8] [INFO] Booting worker with pid: 8
```

### Smoke testing service

As simple as curl-ing (here I use [httpie](https://httpie.org/)):

```bash
$ http 0.0.0.0:5000/health
HTTP/1.1 200 OK
Connection: close
Content-Length: 18
Content-Type: application/json
Date: Fri, 17 Jan 2020 06:59:54 GMT
Server: gunicorn/19.9.0

{
    "message": "OK"
}
```
