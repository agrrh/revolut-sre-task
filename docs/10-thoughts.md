# Conditions of the problem

These are just thoughts to show examiner what things I was thinking off.

Not every point is worth paying attention to in this particular task. Some are OK to skip even in real production environments with lower reliability requirements. We always should remember that each portion of enchancement costs us some resources. And these costs are becoming larger, as we go closer to 100% reliability.

And, come on, I got days for this project. It's a kind if disrespect to give you just a couple lines of code, despite formally it solves the task.

## Monitoring

SRE strongly relies on monitoring, so we need to implement some kind of application health-checks and expose our environment metrics. Right now I'm thinking of packing in Prometheus and Grafana dashboard giving us access to at least few metrics: [4 golden signals](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/)

- **Latency**

  Would collect this from LB (if end up using L7) or calculate p99, p95 and p50 according to web servers metrics.

- **Traffic**

  Expecting LB or underlying web servers to expose it.

- **Errors**

  We may implement small rate of errors both in main app's logic and in health-check (randomly returning 500 sometimes) just to ensure our circuit breakers work fine. We should make this option toggleable and enable it only for non-production mode.

- **Saturation**

  My expectations is that our bottleneck is database I/O limits when it comes to really big amounts of data. Like when dataset comes to 10x larger amounts than DB server's RAM. That's where we supposed to enable sharding or use cache servers like [Redis](https://redis.io/) or [Memcached](https://memcached.org/).

  In reality, we would like to perform some stress/performance testing to find out where our app/environment bottlenecks are.

## Deployment

My main idea is to setup some kind of infrastructure-agnostic platform, like Kubernetes (please, keep in mind, that right now I'm using Nomad) and provide single manifest to deploy application both locally and to the cloud.

To reach this point, I will use following scheme:

- Local deployment via [Minkube](https://kubernetes.io/docs/setup/learning-environment/minikube/) (VirtualBox driver)
- Cloud deployment via [GKE](https://cloud.google.com/kubernetes-engine/)

To ease local testing and following examination (in case you don't have Minkube), I can may also use [docker-compose](https://docs.docker.com/compose/).

## Authentication

Better to say, absence of it. And that's an issue.

One could just flood application with fake data, like "1970-01-01" for most common usernames. This is unacceptable for production usage. We may use things like [JWT](http://jwt.io/) or at least setup some sane rate limits and alerting rules to prevent other user's data corruption.

If that's supposed to be some internal API used by another service (which previously authenticated a client), then it's almost OK. We may just limit access, allowing only trusted resources to reach whole service or potentially destructive endpoints.

## Username specification

Username specification could be more strict.

- Case-sensitivity is unclear

    [RFC 7230](http://tools.ietf.org/html/rfc7230) says, URL path respects it:

    >The scheme and host are case-insensitive and normally provided in lowercase; all other components are compared in a case-sensitive manner.

    So I guess username is implied just as `[A-Za-z]+`, where "A" and "a" are different objects.

- Length limits are unclear, too

    Is "a" a valid username? "aaaa"? "aaaaaaaaaaaaaaaa" (16 letters)? What about 255 occurencies, 1024, 1024^2?

    Different browsers and web servers limit us to different URL sizes, so we probably would like to limit this at App level. For this particular situation 255 seems like a sane limit, assuming there's also scheme, host and some `/api/v1/`-like path prefix.

## Testing

I definitely should provide smoke test and unit tests. Probably, end-to-end tests too (in context of application), or even whole infrastructure-application-deployment system.

## Code quality

I will apply [flake8](http://flake8.pycqa.org/en/latest/) checks and probably some external tool, like [deepsource](https://deepsource.io/).

For [Terraform](https://www.terraform.io/), just using `terraform fmt` and modules feature seems enough. I was using v0.11, so I will try to use v0.12 for the first time now and plan to fall back to v0.11 in case of stucking.
