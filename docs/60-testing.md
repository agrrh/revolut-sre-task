# Testing service

## End-to-end

Let's fill database with my family birthdays:

```raw
Me, Kirill                16.08.1990
My wife, Tatyana          11.05.1993
Our daughter, Victoria    22.10.2019
Our cat, Felix            01.03.2018  # it's a foundling so date is approximate
```

Fun fact is that last date is april fool's day, and Felix really often acts like an idiot :D This week was an exception, he even helped me with part of this task.

![](images/felix-as-consultant.jpg)

None of these dates are used as part of passwords, so it's really safe to publish them:

```bash
$ http PUT http://<load-balancer-ip>/hello/kkovalev dateOfBirth=1990-08-16
...
$ http PUT http://<load-balancer-ip>/hello/tkovaleva dateOfBirth=1993-05-11
...
$ http PUT http://<load-balancer-ip>/hello/vkovaleva dateOfBirth=2019-10-22
...
$ http PUT http://<load-balancer-ip>/hello/felix dateOfBirth=2018-04-01
...
```

Let's check the results:

```bash
$ for i in kkovalev tkovaleva vkovaleva felix; do \
  http --print b http://<load-balancer-ip>/hello/${i}; \
done
{
    "message": "Hello, kkovalev! Your birthday is in 210 day(s)"
}

{
    "message": "Hello, tkovaleva! Your birthday is in 113 day(s)"
}

{
    "message": "Hello, vkovaleva! Your birthday is in 277 day(s)"
}

{
    "message": "Hello, felix! Your birthday is in 73 day(s)"
}
```

All seems good.

## Performance

I would like to check app's performance.

Let's start with 3 concurrent clients, 1 per node:

```bash
$ siege -t 10s -c 3 -b http://<load-balancer-ip>/hello/felix 2>&1 \
  | grep -E 'Transaction|Availability|Concurrency'
Transactions:		         247 hits
Availability:		      100.00 %
Transaction rate:	       26.73 trans/sec
Concurrency:		        2.99
```

It's safe to increase load, let's try 12 clients:

```bash
$ siege -t 10s -c 300 -b http://<load-balancer-ip>/hello/felix 2>&1 \
  | grep -E 'Transaction|Availability|Concurrency'
Transactions:		         964 hits
Availability:		      100.00 %
Transaction rate:	       97.47 trans/sec
Concurrency:		       11.91
```

Still OK, what about 300?

```bash
$ siege -t 10s -c 300 -b http://<load-balancer-ip>/hello/felix 2>&1 \
  | grep -E 'Transaction|Availability|Concurrency'
Transactions:		         975 hits
Availability:		      100.00 %
Transaction rate:	       97.60 trans/sec
Concurrency:		      102.11
```

Really poor results, locally it was around 2000 RPS, now we got awful 100 RPS. For now, time almost depleted so I'm unable to investigate, but I'd say the problem is in poor nodes performance, CPU is single-core and load is around 100%.

## Availability

Let's run a deploy and check if application is available. Cause of chaos engineering feature, we randomly fail ~0.5% of requests. I expect possible availability higher than 99.5% cause failed containers are removed from load balancing with a help of `livenessProbe` parameter.

```bash
# Terminal A
$ kubectl apply -f application/hello-kkovalev.k8s.yml
$ kubectl rollout status deployment/hello-kkovalev

# Terminal B
# started first and ended right after roullout has finised
$ siege -c3 http http://<load-balancer-ip>/hello/felix
...
Transactions:		        2806 hits
Availability:		       99.96 %
Elapsed time:		      115.83 secs
```

Good results, there's some portion of fails, as expected, but it's lower than single instance failure rate.

For client supporting "business logic" retries, availability would be even higher.
