<!-- docs/_sidebar.md -->

- [Task description](00-task.md)
- [Conditions of the problem](10-thoughts.md)
- [Technical stack](20-technical-stack.md)
- [Local environment](30-local-environment.md)
- [Cloud environment](40-cloud-environment.md)
- [Code deploy](50-deploy.md)
- [Architecture and diagrams](55-diagram.md)
- [Testing](60-testing.md)
- [Summarize](70-summarize.md)
