# Architecture and diagrams

## Actual solution

```mermaid
graph LR
  clients((Clients))
  control((Control))

  subgraph GCE
    gce-api(GCE API)

    subgraph europe-west2
      subgraph europe-west2-a
        subgraph kubernetes
          lb[load balancer]

          controller(controller)
          scheduler(scheduler)

          pod1[pod 1]
          pod2[pod ...]
          pod3[pod N]

          controller --> scheduler
        end

        subgraph memorystore
          db1[database master]
        end
      end
    end
  end

  clients -->|HTTP| lb

  lb -->|HTTP| pod1
  lb -->|HTTP| pod2
  lb -->|HTTP| pod3

  pod1 -.-> db1
  pod2 -.-> db1
  pod3 -.-> db1

  control -->|resources management| gce-api

  control -->|service management| controller
```

## Ideal case

### Traffic flow

```mermaid
graph LR
  clients((Clients))
  DNS((DNS))

  subgraph Cloud
    subgraph region-1
      subgraph zone-1a
        cluster-1a
      end

      subgraph zone-1b
        cluster-1b
      end

      cluster-1a -.- cluster-1b
    end

    subgraph region-2
      subgraph zone-2a
        cluster-2a
      end

      subgraph zone-2b
        cluster-2b
      end

      cluster-2a -.- cluster-2b
    end
  end

  clients -.-|geo-based lookup| DNS

  clients -->|HTTPS| cluster-1a
  clients -->|HTTPS| cluster-1b

  clients -->|HTTPS| cluster-2a
  clients -->|HTTPS| cluster-2b
```

### Region architecture

Multiple AZ setups prevent global outages in case of single AZ fails.

We need to think of such decisions in business perspective: does maintenance of complex architectures costs us less or more relative to possible AZ outage losses? Both in finance, effort and reputation risks.

```mermaid
graph LR
  clients((Clients))

  subgraph region-1
    subgraph zone-1a
      subgraph cluster-1a
        lb-1a
        pods-1a

        lb-1a --> |HTTP| pods-1a
      end

      subgraph storage-1a
        db-1a
      end

      pods-1a --> db-1a
    end

    subgraph zone-1b
      subgraph cluster-1b
        lb-1b
        pods-1b

        lb-1b --> |HTTP| pods-1b
      end

      pods-1b --> db-1b
    end

    db-1a -.- |primary-primary| db-1b
  end

  clients --> |HTTPS| lb-1a
  clients --> |HTTPS| lb-1b
```

### Database architecture

There's CAP theoreme, which says, we must choose 2 of 3 following features:

- Consistency, we always get *same* data from any system's node
- Availability, we may always get *some* data, probably not the latest one.
- Partition tolerance, system *survives particular partition outages*.

So depending on our business requirements, we may prefer different approaches:

- Primary-Primary replication between AZs or even regions (e.g. Dynomite when speaking of Redis, or even Tarantool), but I think that's the most complex and unreliable way
- Waiting for multiple acknowledments from different AZs/regions when writing data, while reading mostly from hot cache
- Sharding data between AZs or Regions, to evade global issues in case of single partition outage. Good example is just to keep different databases for EU, US and Asia regions.

Here I propose scheme for:

- sharding data at regions level (Availability, Partition tolerance)
- replication between AZs (Availability, Consistency)

```mermaid
graph LR
  clients((Clients))

  subgraph region-1
    subgraph zone-1a
      subgraph cluster-1a
        pods-1a
      end

      subgraph storage-1a
        db1-pri-1a
        db2-sec-1a
        db3-sec-1a

        db1-pri-1a -.- |replication| db2-sec-1a
        db1-pri-1a -.- |replication| db3-sec-1a
        db2-sec-1a -.- |replication| db3-sec-1a
      end

      pods-1a --> |writes| db1-pri-1a
      pods-1a --> |reads| db2-sec-1a
      pods-1a --> |reads| db3-sec-1a
    end
  end

  subgraph region-1
    subgraph zone-1b
      subgraph cluster-1b
        pods-1b
      end

      subgraph storage-1b
        db1-pri-1b
        db2-sec-1b
        db3-sec-1b

        db1-pri-1b -.- |replication| db2-sec-1b
        db1-pri-1b -.- |replication| db3-sec-1b
        db2-sec-1b -.- |replication| db3-sec-1b
      end

      pods-1b --> |writes| db1-pri-1b
      pods-1b --> |reads| db2-sec-1b
      pods-1b --> |reads| db3-sec-1b
    end
  end

  db1-pri-1a === |primary-primary| db1-pri-1b

  clients --- pods-1a
  clients --- pods-1b
```
