# Code deploy

## Local

For local environment process was described in previous chapter, [Local environment](./30-local-environment.md).

In general, it's as simple as running following command:

```bash
$ make app-deploy-local
```

## Cloud

For cloud environment, we need some additional actions over configuration we got after configuring local one.

In the same terminal, we operated over TF:

```bash
$ make app-deploy-cloud
```

Which leads to following

```bash
$ gcloud container clusters get-credentials main-gke-cluster \
  --zone europe-west2-a \
  --project ${TF_VAR_project}

$ kubectl apply -f application/hello-kkovalev.k8s.yml

$ kubectl rollout status deployment/hello-kkovalev

$ kubectl get service hello-kkovalev
NAME             TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)        AGE
hello-kkovalev   LoadBalancer   ??.???.9.105   ??.??.39.244   80:32313/TCP   30m
```
