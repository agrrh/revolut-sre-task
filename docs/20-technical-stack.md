# Technical stack

## Application

- [Python](https://www.python.org/) - my weapon of choice for now
    - [Flask](https://www.palletsprojects.com/p/flask/) - well-known HTTP framework reduces code ownership grade
    - [Flask-RESTPlus](https://flask-restplus.readthedocs.io/) - I'm using it as new technology, to give it a try and learn something new. Supposed to make code more extendable and ease routines like docs generation.
    - [Gunicorn](https://gunicorn.org/) - WSGI library, needed since Flask states that it's development werkzeug-based server is not designed for production

## Storage

- [Redis](https://redis.io/) - simple KV storage

## Deployment

- [Makefile](http://www.gnu.org/software/make/manual/) - used as an entrypoint to project deployment
- [Docker](https://www.docker.com/) - solution to make application portable

### Local

- [Minkube](https://kubernetes.io/docs/setup/learning-environment/minikube/) - pretending my laptop is a cluster

### Cloud

- [Terraform](https://www.terraform.io/) - IaC tool, pretty standard nowadays

For Google Cloud Engine:

- [GKE](https://cloud.google.com/kubernetes-engine/) - k8s as a service
- [Memorystore](https://cloud.google.com/memorystore/) - cloud Redis
