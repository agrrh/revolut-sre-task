# Cloud environment

I'm using [GCE](https://cloud.google.com/) and [Terraform](https://www.terraform.io/) here.

## Overview

```mermaid
graph LR
  tf-cli[TF CLI]

  subgraph GCE
    subgraph Organization
      subgraph "admin project"
        acc-tf[service account]
      end

      subgraph "kkovalev project"
        proj-kkovalev((project))
        gke-cluster
      end
    end
  end

  tf-cli --> acc-tf
  acc-tf --> proj-kkovalev
  proj-kkovalev --> gke-cluster
```

## Preparations

Assuming Terraform 0.12 is installed.

One need to manually create GCE entities to let TF operate cloud resources

- Create organization
- Create Admin project inside it and service account named e.g. "terraform"
- Save credentials to JSON file and set up `GOOGLE_APPLICATION_CREDENTIALS` environment variable pointing to that file

More details could be found [here](https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform). Here's tutorial on how I did so:

1. Created organization via [Google Cloud Identity](https://cloud.google.com/identity/)

2. Created an admin project, (setting variable names so later it's possible to re-use them in TF):

    ```bash
    # Change to actual values
    $ export TF_VAR_org_id=0
    $ export TF_VAR_credentials="/home/${USER}/.config/gcloud/kkovalev-terraform-admin.json"
    $ export TF_VAR_billing_account=000AAA-111BBB-333CCC

    # Assume random is lucky enough
    $ export TF_VAR_project="kkovalev-admin-$(cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | head --bytes 6)"

    $ gcloud projects create ${TF_VAR_project} \
      --organization ${TF_VAR_org_id}

    $ gcloud beta billing projects link ${TF_VAR_project} \
      --billing-account ${TF_VAR_billing_account}

    $ gcloud config set project ${TF_VAR_project}
    ```

2.  Created service account:

    ```bash
    $ gcloud iam service-accounts create terraform \
      --display-name "Terraform admin for ${TF_VAR_project}"

    $ gcloud iam service-accounts keys create ${TF_VAR_credentials} \
      --iam-account terraform@${TF_VAR_project}.iam.gserviceaccount.com

    $ gcloud projects add-iam-policy-binding ${TF_VAR_project} \
      --member serviceAccount:terraform@${TF_VAR_project}.iam.gserviceaccount.com \
      --role roles/viewer

    $ for service in cloudresourcemanager.googleapis.com cloudbilling.googleapis.com iam.googleapis.com compute.googleapis.com container.googleapis.com; do
      gcloud services enable ${service};
    done

    $ gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} \
      --member serviceAccount:terraform@${TF_VAR_project}.iam.gserviceaccount.com \
      --role roles/resourcemanager.projectCreator

    $ gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} \
      --member serviceAccount:terraform@${TF_VAR_project}.iam.gserviceaccount.com \
      --role roles/billing.user
    ```

## Applying

At this point, it's possible to operate GCE resources via Terraform.

It's a good practice to use S3 or other remote storage to save state files, but for this particular showcase, we will use just local files cause of simplicity reasons.

After initializing TF drivers and modules, we may apply changes to our infrastructure:

```bash
$ terraform init
...
$ terraform apply
...
```

Ensure that cluster is up:

```bash
$ gcloud container clusters list
NAME              LOCATION        MASTER_VERSION  MASTER_IP       MACHINE_TYPE   NODE_VERSION    NUM_NODES  STATUS
main-gke-cluster  europe-west2-a  1.13.11-gke.14  ??.???.138.192  n1-standard-1  1.13.11-gke.14  3          RUNNING
```

Great, now we may head to deploying the code.

## Destroying

It's possible as backward rewind:

```bash
$ terraform destroy
...

$ gcloud projects delete ${TF_VAR_project}
...
```

Also, check GCE dashboard to ensure you removed service user and other related stuff.
