# Task description

## Part 1

Design and code simple "Hello, World!" application that exposes the following HTTP-based APIs:

#### PUT /hello/{username}

**Description**
Saves/updates the given user's name and date of birth in the database.

```raw
PUT /hello/{username}
{
  "dateOfBirth": "YYYY-MM-DD"
}
```

**Response**
```raw
204 No Content
```

**Notes**

- `{username}` must contain only letters
- `YYYY-MM-DD` must be a date before the today date

#### GET /hello/{username}

**Description**
Returns hello birthday message for the given user.

**Request**
```raw
GET /hello/{username}
```

**Response**

1. Username's birthday is in N days:

    ```raw
    200 OK
    {
      "message": "Hello, <username>! Your birthday is in N day(s)"
    }
    ```

2. Username's birthday is today:

    ```raw
    200 OK
    {
      "message": "Hello, <username>! Happy birthday!"
    }
    ```

## Part 2

Produce a system diagram of your solution deployed to either AWS or GCP.

## Part 3

Write configuration scripts for building and no-downtime production deployment of this application, keeping in mind aspects that SRE would have to consider.

# Implicit requirements

1. Code produced by you expected to be of high quality.
2. The solution must have tests, runnable locally and deployable in the cloud.
3. Use common sense.
