# Summarize

I've failed to get some things in time, since really had only 2 or 3 evenings out of 7 to concentrate on the work.

That's a concequence of my poor strategic decision to use GCE and Kubernetes, which I was not quite as familiar, as I would be happy to be. Huge portion of time was spent reading documentation and seeking for solutions of minor issues.

Using Nomad on AWS EC2, I would spent near-to-0 time organizing fault-tolerant cluster (since I already have trusted modules and roles) and thus would be able to invest more time to work on: database, some packed in CI/CD or monitoring.

Let's go through main parts, to provide final assessment:

* Application

  * Code quality
    ```raw
    + Architecture is pretty good for small app, it's extendable
    + Code covered with tests, especially doctests
    + No warnings on code standards
    - Failed to attach external style checks, as intended
    ? Not transactional nature would lead to problems when it comes to complex business scenarios

    = GOOD, I'm satisfied with this small API
    ```

  * Performance
    ```raw
    + Locally shows 2k RPS
    ? Really bad in cloud, but cause wasn't investigated

    = GOOD, I'm almost sure issues was related to single-core CPUs, not application itself
    ```

  * Data storage
    ```raw
    + Redis made it dead simple to implement
    + Cause of good architecture, Redis could be easily replaced
    - ! Single-instance setup is not fault tolerant at all
    - Got not time to migrate to PostgreSQL
    ? Built database on top of cloud service, not as cluster workload. I believe we may try this approach when 2 conditions are met:
      * business requirement to make product totally provider-agnostic
      * impressive understanding of controller/scheduler behavior to evade naive outages

    = OK, that choice saved me enough time to deal with GCE and k8s, despite made solution less advanced
    ```

* Platform

  * GCE
    ```raw
    + Proved Infrastructure as Code approach, despite storing state locally
    + Tried TF v0.12 for the first time
    + Learned more about GCE ideas and nuances
    - It took almost all of the time

    = BAD, I should not choose more than 1 infamiliar thechnology at once
    ```

  * Kubernetes
    ```raw
    + Made me much more familiar with Kubernetes in general
    - Also ate great portion of time
    - My skills related to Hashicorp stack stayed unwanted
    - Failed to reach load balancer setup, at this stage I could work on TLS, Auth and Rate Limiting, as intended

    = OK, proved that I can learn quickly, but, as said before, I should not choose more than 1 infamiliar thechnology at once
    ```

  * Deployment
    ```raw
    + Zero-downtime deployment is a must
    + Work done on Makefile allow us to easy integrate into CI/CD processes
    - Got no time to pack configuration variables in right manner (ConfigMap), for now one need to edit manifest directly
    - Not found suitable place for Ansible, so wasn't able to show my experise

    = GOOD, pros are solid and cons are easy to fix
    ```

  * Fault-tolerance
    ```raw
    + Good enough for non-critical service, 3 nodes are required minimum
    - Ran out of time, ended up with single-AZ deployment

    = BAD, could not prove my understanding of scalable architecture
    ```

* Related stuff

  * Observability
    ```raw
    + Well, at least I state that this is important part
    - Logs are just human-readable text, when they should be more like JSON data objects to ease further analyzing
    - No advanced logs collection like ELK, only default GCE feature
    - No metrics implemented, therefore no metrics collection, too

    = BAD, could not prove my ability to set up good observability
    ```

  * CI/CD
    ```raw
    + Basic Makefile automations
    - Not used cloud code storage and CI/CD features
    - It's also possible to pack-in e.g. Drone instance manifest to make project really portable and provider-agnostic

    = OK, I doubt anyone went this far in just a week
    ```

## Overall

I definitely should invest time in more familiar solutions to complete more goals. Instead, I decided to impress you with my ability to learn quckly as for me it's really important, especially for relocation cases. Let's see if it was a right stake.

For couple of days I made really good progress with perspective thechnologies and greatly increased my knowledge horizons.

That was an interesting project and I'm satisfied how results pushed me forward and brought me new ideas.

Really dialectics case, in russian, we say "палка о двух концах" (something like "double-edged weapon").
