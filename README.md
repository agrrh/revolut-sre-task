# SRE task

Repository with both task and solution.

To start, please run:

```bash
make docs-run
```

And then visit local docs page to start our journey:

[Local docs container](http://0.0.0.0:5001/)
