import json
import logging
import os

from hello.storage.redis import Redis
from hello.storage.mock import Mock

from hello.model.username import Username


class Storage(object):
    """Interface to read/write data.

        >>> storage = Storage(mock=True)
    """

    def __init__(self, mock: bool = False, host: str = 'localhost', port: int = 6379):
        self.logger = logging.getLogger(__name__)

        # Change this to implement:
        # - Other engines
        # - Some additional cache layer
        self.handler = (
            Redis(
                host=os.environ.get('HELLO_STORAGE_HOST', 'localhost'),
                port=os.environ.get('HELLO_STORAGE_PORT', '6379'),
            )
            if not mock
            else Mock()
        )

    def write(self, username):
        """Writes object to database.

            >>> from hello.model.username import Username
            >>> storage = Storage(mock=True)
            >>> storage.write(Username(id='foo', raw="{'foo': 1}"))
            True
        """
        return self.handler.write(
            username.id,
            json.dumps(username.raw)
        )

    def read(self, username_id):
        """Reads object from database.

            >>> storage = Storage(mock=True)
            >>> storage.write(Username(id='foo', raw="{'foo': 1}"))
            True
            >>> storage.read('foo').valid
            True
            >>> storage.read('bar')
            False
        """
        raw = self.handler.read(username_id)

        try:
            raw = json.loads(raw)
        except Exception:
            self.logger.error(f'Could not load data as JSON: {raw}')
            raw = {}

        if raw:
            return Username(id=username_id, raw=raw)

        return False

    def delete(self, username_id):
        """Removes object from database.

            >>> storage = Storage(mock=True)
            >>> storage.delete('foo')
            True
            >>> storage.read('foo')
            False
        """
        return self.handler.delete(username_id)
