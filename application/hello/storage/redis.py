import redis


class Redis(object):
    """Redis driver."""

    def __init__(self, **kwargs):
        self.handler = redis.Redis(
            host=kwargs.get('host', 'localhost'),
            port=kwargs.get('port', 6379),
        )

    def write(self, key, data):
        """Writes object to database."""
        return self.handler.set(key, data)

    def read(self, key):
        """Reads object from database."""
        return self.handler.get(key)

    def delete(self, key):
        """Remove object from database."""
        return self.handler.delete(key)
