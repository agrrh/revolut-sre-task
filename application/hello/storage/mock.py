class Mock(object):
    """Mock driver."""

    def __init__(self):
        self.data = {}

    def write(self, key, data):
        """Writes object to memory.

            >>> mock = Mock()

            >>> mock.write('foo', 42)
            True
        """
        self.data[key] = data
        return key in self.data

    def read(self, key):
        """Reads object from memory.

            >>> mock = Mock()

            >>> mock.write('foo', 42)
            True
            >>> mock.read('foo')
            42

            >>> mock.write('bar', 'baz')
            True
            >>> mock.read('bar')
            'baz'
        """
        return self.data.get(key) or False

    def delete(self, key):
        """Remove object from database.

            >>> mock = Mock()

            >>> mock.write('bar', 'baz')
            True
            >>> mock.delete('bar')
            True
            >>> mock.read('bar')
            False
        """
        if key in self.data:
            del self.data[key]
        return True
