import logging


class Model(object):
    """Base Model object, implementing various default properties."""

    def __init__(self, raw=None):
        self.logger = logging.getLogger(__name__)
        self.raw = raw

    def __repr__(self):
        return str(self.raw)

    @property
    def valid(self):
        """Represents if object is valid."""
        return True
