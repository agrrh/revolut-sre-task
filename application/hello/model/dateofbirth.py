import datetime

from hello.model import Model


class DateOfBirth(Model):
    """Username object."""

    def __init__(self, raw: str = None):
        super(DateOfBirth, self).__init__(raw=raw)

    @property
    def dt(self):
        dt = None

        try:
            dt = datetime.datetime.strptime(self.raw, '%Y-%m-%d')
        except Exception as e:
            self.logger.error(f'Could not parse date notation, expected YYYY-MM-DD, got: "{self.raw}"')

        self.logger.debug(f'Date notation is "{dt}"')

        return dt

    @property
    def valid(self):
        """Represents if object is valid:

            >>> DateOfBirth('1970-01-01').valid
            True
            >>> DateOfBirth(f'{datetime.date.today().year + 1}-01-01').valid
            False
        """
        if not self.dt:
            self.logger.warning(f'DateOfBirth notation is absent, nothing to parse')
            return False

        if self.dt > datetime.datetime.today():
            self.logger.warning(f'DateOfBirth is in future: {self.dt}')
            return False

        return True

    @property
    def anniversary_days(self):
        """Returns amount of days left to next anniversary. 0 means it's today.

        Supposed to be in a year's range from now:

            >>> DateOfBirth(f'1970-01-01').anniversary_days in range(0, 367)
            True

        Today's check:

            >>> DateOfBirth(f'1970-{datetime.date.today().month}-{datetime.date.today().day}').anniversary_days
            0
        """
        now = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

        # Get date for this year (or next, if already occured)
        date = self.dt.replace(year=now.year)
        if date < now:
            date = date.replace(year=now.year + 1)

        # Return days from now to date of interest
        return (date - now).days
