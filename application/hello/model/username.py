import re

from hello.model import Model
from hello.model.dateofbirth import DateOfBirth


class Username(Model):
    """Username object."""

    def __init__(self, id: str = None, raw: dict = {}):
        super(Username, self).__init__(raw=raw)
        self.id = id

    def __repr__(self):
        return self.id

    @property
    def dateofbirth(self):
        return DateOfBirth(raw=self.raw.get('dateOfBirth'))

    @property
    def valid(self):
        """Represents if object is valid:

            >>> Username(id=None).valid
            False
            >>> Username(id=0x00).valid
            False
            >>> Username(id=-1.0).valid
            False
            >>> Username(id='').valid
            False
            >>> Username(id='foo1').valid
            False
            >>> Username(id='foo%').valid
            False
            >>> Username(id='foo bar').valid
            False
            >>> Username(id='foo').valid
            True
            >>> Username(id='Bar').valid
            True
            >>> Username(id='a' * 255).valid
            True
            >>> Username(id='a' * 256).valid
            False
            >>> Username(id='a' * 10_000).valid
            False
        """
        if type(self.id) is str and len(self.id) > 255:
            return False

        pattern = r'^[A-Za-z]+$'

        try:
            return bool(re.match(pattern, self.id))
        except TypeError:
            self.logger.error(f'Object type is not string-like: {type(self.id)} "{self.id}"')

        return False
