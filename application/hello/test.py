import datetime

from hello import app


def test():
    # Healthcheck is alive most of the times
    assert 200 in [
        app.test_client().get('/health').status_code
        for _
        in range(5)
    ]
    # Ensure some chaos engineering failures is there
    assert 500 in [
        app.test_client().get('/health').status_code
        for _
        in range(1000)
    ]

    # Metrics
    assert app.test_client().get('/metrics').status_code == 200

    # Empty username
    assert app.test_client().put(
        '/hello/',
        json={"dateOfBirth": f"{datetime.date.today().year + 1}-01-01"}
    ).status_code == 404

    # Wrong usernames
    assert app.test_client().put(
        '/hello/123',
        json={"dateOfBirth": f"{datetime.date.today().year + 1}-01-01"}
    ).status_code == 400
    assert app.test_client().put(
        '/hello/😋',
        json={"dateOfBirth": f"{datetime.date.today().year + 1}-01-01"}
    ).status_code == 400
    assert app.test_client().put(
        '/hello/contains whitespaces',
        json={"dateOfBirth": f"{datetime.date.today().year + 1}-01-01"}
    ).status_code == 400

    # Non-JSON data
    assert app.test_client().put('/hello/foo', json='bar').status_code == 400

    # Date in the future
    assert app.test_client().put(
        '/hello/foo',
        json={"dateOfBirth": f"{datetime.date.today().year + 1}-01-01"}
    ).status_code == 400

    # Correct data
    assert app.test_client().put(
        '/hello/foo',
        json={"dateOfBirth": "1970-01-01"}
    ).status_code == 204

    # Actual "business" logic
    tommorrow_year_ago = datetime.date.today() - datetime.timedelta(days=1)
    assert app.test_client().put(
        '/hello/tommorrow',
        json={"dateOfBirth": f"{tommorrow_year_ago.strftime('%Y-%m-%d')}"}
    ).status_code == 204
    assert app.test_client().get('/hello/tommorrow').status_code == 200
