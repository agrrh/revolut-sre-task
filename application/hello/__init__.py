import logging
import os
import uuid
import random

from flask import Flask, request
from flask_restplus import Api, Resource

from hello.storage import Storage
from hello.model.username import Username


logger = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app, doc=False)

# I'm pretty sure it's a bad idea to hack code like that for unit testing
# If there's a better way, please let me know
storage_ = Storage(mock=bool(os.environ.get('HELLO_TEST')))

# Random App instance UUID
app_uuid = str(uuid.uuid4())


@api.route('/health')
class Health(Resource):
    def get(self):
        # This is "guaranteed" to not collide:
        # - UUID is random enough
        # - External user unable to use dashes
        username = Username(
            id=f'health-{app_uuid}',
            raw={
                'dateOfBirth': '1970-01-01'
            }
        )

        try:
            if random.randint(0, 1000) < 5:
                logger.warning(f'Health check intentionally failed')
                raise Exception('Chaos engineering')

            # FIXME This should be a transaction when application goes to complex (non-atomic) DB operations
            storage_.write(username)
            storage_.read(username.id)
            storage_.delete(username.id)
        except Exception as e:
            logger.error(f'Health check error: "{e}"')
            return {
                'message': 'Business logic unavailable'
            }, 500

        return {
            'message': 'OK'
        }, 200


@api.route('/metrics')
class Metrics(Resource):
    def get(self):
        return {
            'message': 'Not available for now'
        }, 200


@api.route('/hello/<string:username_id>')
class Hello(Resource):
    def get(self, username_id):
        try:
            username = storage_.read(username_id)
        except Exception as e:
            logger.critical(f'Could not load data from database: "{e}"')
            return {'message': f'Could not load data from database'}, 500

        if not username:
            return {'message': f'Username "{username_id}" not found'}, 404

        if not username.valid:
            return {'message': f'Username "{username_id}" is not valid'}, 400

        if username.dateofbirth.anniversary_days > 0:
            return {
                'message': f'Hello, {username.id}! Your birthday is in {username.dateofbirth.anniversary_days} day(s)'
            }, 200
        else:
            return {'message': f'Hello, {username.id}! Happy birthday!'}, 200

        return {'message': f'Something probably went wrong'}, 500

    def put(self, username_id):
        username = Username(
            id=username_id,
            raw=request.json
        )

        if type(request.json) is not dict:
            return {
                'message': f'Could not parse data you sent'
            }, 400

        if not username.valid:
            return {
                'message': f'Username "{username.id}" is not valid'
            }, 400

        if not username.dateofbirth.valid:
            return {
                'message': f'Date of birth is not valid: {username.dateofbirth}'
            }, 400

        try:
            storage_.write(username)
        except Exception as e:
            logger.critical(f'Could not write data to database: "{e}"')
            return {
                'message': f'Could not write data to database'
            }, 500

        return {}, 204
